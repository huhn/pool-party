Rails.application.routes.draw do
  resources :pools do
    get :instance_config
    get :close_pool
    get :open_pool
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'pools#index'
end
