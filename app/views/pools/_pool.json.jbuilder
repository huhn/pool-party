json.extract! pool, :id, :title, :pool_url, :logo_url, :created_at, :updated_at
json.url pool_url(pool, format: :json)
