class Pool < ApplicationRecord

  def create_piri!
    require 'docker'
    self.port = new_piri_port
    self.pool_url = "#{ROOT_URL}:#{port}"
    pp "Creating piri container for #{title} with port: #{port}"

    container = Docker::Container.create(
        'Image' => 'chrisss404/piri:latest',
        'ExposedPorts' => { '8080/tcp' => {} },
        'HostConfig' => {
          'PortBindings' => {
            '8080/tcp' => [{ 'HostPort' => port}]
          }
        }
      )
    self.container_id = container.id

    container.start
    self.save!
  end

  def create_ui!
    require 'docker'
    self.ui_port = new_ui_port
    pp "Creating ui container for #{title} with port: #{ui_port}"

    container = Docker::Container.create(
        'Image' => 'registry.gitlab.com/huhn/piri-web:latest',
        #'Image' => 'piri-web:latest',
        'Env'=> ["TITLE='#{title}'", "LOGO_URL='#{logo_url}'", "POOL_URL='#{pool_url}'"],
        'ExposedPorts' => { '3000/tcp' => {} },
        'HostConfig' => {
          'PortBindings' => {
            '3000/tcp' => [{ 'HostPort' => ui_port}]
          }
        }
      )
    self.container_ui_id = container.id

    pp 'cotainter'
    pp container.to_s
    pp "container.json"
    pp container.json["Config"]["Env"]
    container.start
    self.save!
  end

  def stop_piri!
    container = Docker::Container.get(container_id)
    container.stop
    self.pool_url = nil
    self.container_id = nil
    save!
  end

  def new_piri_port

    # use port 30001-39999 TODO: Fix if we go big ;)
    # we start with 1 to match with ids
    return "30#{(Pool.all.count + 1).to_s.rjust(3, '0')}"

  end

  def new_ui_port

    # use port 30001-39999 TODO: Fix if we go big ;)
    # we start with 1 to match with ids
    return "40#{(Pool.all.count + 1).to_s.rjust(3, '0')}"

  end

end
