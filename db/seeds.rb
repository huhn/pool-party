# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

REMOTE_POOLS = [
  {
    title: "einfach-iota.de",
    pool_url: "https://nutzdoch.einfach-iota.de",
    logo_url: 'https://einfach-iota.de/content/uploads/2018/09/einfachiota-logo.png',
    ui: 'yes'
  },
  {
    title: "trytes.eu",
    pool_url: "https://pool.trytes.eu",
    ui: 'yes'
  },

]

HOSTET_POOLS = [
  {
    title: "iota.saarland",
    logo_url: 'https://cdn.discordapp.com/attachments/470157228414861314/476510470841434114/mus_a1.png',
    ui: 'yes'
  },
]




def create_remote_pool pool_attrs

  pool = Pool.create(pool_attrs)
  pool.create_ui!
  pool
end


REMOTE_POOLS.each do |remote_pool|
  create_remote_pool remote_pool
end

HOSTET_POOLS.each do |remote_pool|
  pool = create_remote_pool remote_pool
  pool.create_piri!
end
