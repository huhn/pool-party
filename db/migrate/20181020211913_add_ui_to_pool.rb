class AddUiToPool < ActiveRecord::Migration[5.2]
  def change
    add_column :pools, :ui, :string
    add_column :pools, :ui_port, :string
    add_column :pools, :container_ui_id, :string
  end
end
