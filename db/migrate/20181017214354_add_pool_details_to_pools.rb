class AddPoolDetailsToPools < ActiveRecord::Migration[5.2]
  def change
    add_column :pools, :port, :string
    add_column :pools, :container_id, :string
  end
end
