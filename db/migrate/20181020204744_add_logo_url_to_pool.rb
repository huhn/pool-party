class AddLogoUrlToPool < ActiveRecord::Migration[5.2]
  def change
    add_column :pools, :logo_url, :string
  end
end
