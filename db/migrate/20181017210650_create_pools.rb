class CreatePools < ActiveRecord::Migration[5.2]
  def change
    create_table :pools do |t|
      t.string :title
      t.string :pool_url

      t.timestamps
    end
  end
end
